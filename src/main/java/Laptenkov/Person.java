package Laptenkov;

/**
 * Класс {@link Person} реализует интерфейс Comparable<Person>,
 * определить метод toString
 * Реализовать equals & hashCode
 * Гарантируется, что значения name, city не null
 * Условие равенства: все поля name, city, age должны совпадать.
 * (name и city без учета регистра)
 */
public class Person {
}
